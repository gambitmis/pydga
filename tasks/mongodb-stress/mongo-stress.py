from dotenv import load_dotenv
from pymongo import MongoClient
import datetime
from faker import Faker
import os
import random

load_dotenv()

default_loop_count = 10

fake = Faker()

def fakerPeople(num):
    peoples = []
    fake = Faker()
    for _ in range(num):
        paragraph_size = random.randint(5,20)
        people = {
            'id': fake.msisdn(),
            'name': fake.name(),
            'address': fake.address(),
            'job': fake.job(),
            'company': fake.company(),
            'telephone': fake.phone_number(),
            'car_id': fake.license_plate(),
            'desc': fake.paragraph(nb_sentences=paragraph_size),
            'last_updated': datetime.datetime.utcnow()
        }
        peoples.append(people)
    return peoples

def addMongodb(data):
    try:
        db = client.citizen
        citizen_collection = db.prfiles
        result = citizen_collection.insert_many(data)
        document_ids = result.inserted_ids
        print("# of documents inserted: " + str(len(document_ids)))
        print(f"_ids of inserted documents: {document_ids}")

    except Exception as e:
        print(e)

try:
  if os.getenv("MONGO_URI"):
      mongo_uri = os.getenv("MONGO_URI")
      client = MongoClient(mongo_uri)
      try:
        res = client.admin.command('ping')
        print("Pinged your deployment. You successfully connected to MongoDB!")
      except Exception as e:
        raise ValueError(e)

      job_count = int(os.getenv('JOB_COUNT', default_loop_count))
      plan = os.getenv("PLAN","sim1")
      
      print(f"TASK SUMMARY: RUN {job_count} TIMES with {plan}")
      
      task = 1
      if plan == "sim1":
        for _ in range(job_count):
            peoples = fakerPeople(1)
            print(peoples)
            addMongodb(peoples)
            print(f"task: {task} finish time: {datetime.datetime.utcnow()} type: sim1 ")
            task += 1
      elif plan == "sim2":
        print("sim2")
        for _ in range(job_count):
            randomFake = random.randint(5,20)
            peoples = fakerPeople(randomFake)
            addMongodb(peoples)
            print(f"task: {task} finish time: {datetime.datetime.utcnow()} type: sim2 ")
            task += 1
      elif plan == "sim3":
          print("sim3")
      else:
          raise ValueError("Wrong Plan")
      client.close()
  else:
      raise ValueError("MONGO_URI is not Define Please Define in .env")
except Exception as e:
   print(f"Error: {e}")



