from flask import Flask, render_template, request, jsonify, json, Response, redirect, url_for
from faker import Faker
from flask_sqlalchemy import SQLAlchemy
import requests
import random
import platform
import socket
import datetime
import subprocess
import psutil  # Import the psutil library

app = Flask(__name__)

def get_public_ip():
    try:
        response = requests.get('http://ifconfig.me/ip')
        if response.status_code == 200:
            return response.text.strip()
        else:
            print(f"Failed to retrieve IP address. Status code: {response.status_code}")
    except Exception as e:
        print(f"Error: {e}")
        
def getSystemInfo():
    ip_addr = socket.gethostbyname(socket.gethostname())
    hostname = socket.gethostname()
    current_time = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    cpu_usage = psutil.cpu_percent()  # Get CPU usage percentage
    memory_info = psutil.virtual_memory()  # Get memory usage information
    cpu_core = psutil.cpu_count()

    system_info = {
        'IP Address': ip_addr,
        'Hostname': hostname,
        'OS': platform.platform(),
        'Current Time': current_time,
        'CPU Core': cpu_core,
        'CPU Usage': cpu_usage,  # Add CPU usage to system_info
        'Memory Usage': memory_info.percent,  # Add memory usage to system_info
        'total_memory': f"{memory_info.total / (1024 ** 3):.2f} GB"
    }
    return system_info

def fakerPeople(num):
    peoples = []
    fake = Faker()
    for _ in range(num):
        paragraph_size = random.randint(5,20)
        people = {
            'id': fake.msisdn(),
            'name': fake.name(),
            'address': fake.address(),
            'job': fake.job(),
            'company': fake.company(),
            'telephone': fake.phone_number(),
            'car_id': fake.license_plate(),
            'desc': fake.paragraph(nb_sentences=paragraph_size)
        }
        peoples.append(people)
    return peoples

@app.route('/')
def index():
    navbar = "main"
    system_info = getSystemInfo()
    mypublic_ip = get_public_ip()
    return render_template('index.html',system_info=system_info,mypublic_ip=mypublic_ip,navbar=navbar)

if __name__ == '__main__':
    app.run(host="0.0.0.0",port=5000,debug=True)