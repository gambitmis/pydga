#!/usr/bin/env python
import os
from faker import Faker
from dotenv import load_dotenv
from pymongo import MongoClient
import platform
import socket
from datetime import datetime
import psutil
import requests
import random

fake = Faker()
load_dotenv()

default_loop_count = 10

def get_public_ip():
    try:
        response = requests.get('http://ifconfig.me/ip')
        if response.status_code == 200:
            return response.text.strip()
        else:
            print(f"Failed to retrieve IP address. Status code: {response.status_code}")
    except Exception as e:
        print(f"Error: {e}")

def fakerPeople(num):
    peoples = []
    fake = Faker()
    for _ in range(num):
        paragraph_size = random.randint(5,20)
        people = {
            'id': fake.msisdn(),
            'name': fake.name(),
            'address': fake.address(),
            'job': fake.job(),
            'company': fake.company(),
            'telephone': fake.phone_number(),
            'car_id': fake.license_plate(),
            'desc': fake.paragraph(nb_sentences=paragraph_size),
            'last_updated': datetime.datetime.utcnow()
        }
        peoples.append(people)
    return peoples

def getSystemInfo():
    ip_addr = socket.gethostbyname(socket.gethostname())
    hostname = socket.gethostname()
    current_time = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    cpu_usage = psutil.cpu_percent()  # Get CPU usage percentage
    memory_info = psutil.virtual_memory()  # Get memory usage information
    cpu_core = psutil.cpu_count()

    system_info = {
        'IP Address': ip_addr,
        'Hostname': hostname,
        'OS': platform.platform(),
        'Current Time': current_time,
        'CPU Core': cpu_core,
        'CPU Usage': cpu_usage, 
        'Memory Usage': memory_info.percent,
        'total_memory': f"{memory_info.total / (1024 ** 3):.2f} GB"
    }
    return system_info

def addMongodb(data):
    try:
        client.admin.command('ping')
        print("Pinged your deployment. You successfully connected to MongoDB!")
        db = client.citizen
        citizen_collection = db.prfiles
        result = citizen_collection.insert_many(data)
        document_ids = result.inserted_ids
        print("# of documents inserted: " + str(len(document_ids)))
        print(f"_ids of inserted documents: {document_ids}")
        client.close()
    except Exception as e:
        print(e)

if os.getenv("MONGO_URI"):
    
    mongo_uri = os.getenv("MONGO_URI")
    loop_count = int(os.getenv('LOOP_COUNT', default_loop_count))
    insert_method = os.getenv("INSERT_METHOD","once")
    print(f"Task connect to {mongo_uri} for {loop_count} times which insert {insert_method} data per time")
    systemInfo = getSystemInfo()
    print(f"Hostname: {systemInfo['Hostname']}")
    print(f"IP Address: {systemInfo['IP Address']}")
    #print(f"OS: {systemInfo['OS']}")
    pubIP = get_public_ip()
    print(f"Public IP: {pubIP}")
    print(f"Current Time: {systemInfo['Current Time']}")
    client = MongoClient(mongo_uri)
    if insert_method == "many":
        print("many")
        peoples = fakerPeople(loop_count)
        print(peoples)
        addMongodb(peoples)
        
    elif insert_method == "once":
        print("once")
        people = fakerPeople(1)
        print(people)
        addMongodb(people)
    else:
        raise ValueError("Alert !!! InsertMethod must be only Once or Many")
    
else:
    raise ValueError("MONGO_URI is not Define Please Define in .env")



